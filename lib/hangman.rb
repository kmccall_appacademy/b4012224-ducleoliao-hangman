require 'byebug'
class Hangman
  attr_reader :guesser, :referee
  attr_accessor :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    guesser.register_secret_length(secret_word_length)
    @board = Array.new(secret_word_length)
  end

  def take_turn
    guess = guesser.guess(@board)
    guess_idx = @referee.check_guess(guess)
    update_board(guess, guess_idx)
    guesser.handle_response(guess, guess_idx)
  end

  def update_board(guess, guess_idx)
    guess_idx.each { |i| board[i] = guess }
  end

end

class HumanPlayer
end

class ComputerPlayer
attr_reader :dictionary
attr_accessor :secret_word

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def candidate_words
    @candidate_words
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    @secret_word.length
  end

  def check_guess(ch)
    letters = []
    @secret_word.chars.each_with_index { |letter, idx| letters << idx if letter == ch }
    letters
  end

  def register_secret_length(word_length)
    @candidate_words = []
    @dictionary.each { |word| @candidate_words << word if word.length == word_length }
    @candidate_words
  end

  def guess(board)
    most_common = Hash.new(0)
    @candidate_words.join.each_char { |ch| most_common[ch] +=1 }
    most_common = most_common.sort_by { |k,v| v }

    most_common_letter = most_common.pop[0]
    if board.include?(most_common_letter)
      most_common_letter = most_common.pop[0]
    else
      most_common_letter
    end
  end

  def handle_response(letter, guess_idx)
    if guess_idx.empty?
      @candidate_words.reject! { |word| word.each_char.any? { |ch| ch == letter } }
    else
      @candidate_words.select! { |word| guess_idx.all? { |idx| word[idx] == letter && word.count(letter) == guess_idx.size } }
    end

    @candidate_words
  end

end
